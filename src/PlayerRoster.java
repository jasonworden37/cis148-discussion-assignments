/*
   Name: Jason Worden
   Date: 10/03.2018
*/

import java.util.Scanner;

public class PlayerRoster {
    public static void main(String[] args) {
        Scanner scnr = new Scanner(System.in);
        int[] jersey = new int[5];
        int[] rating = new int[5];
        int temp = 0;
        char menu = 'a';
        for (int i = 0; i < jersey.length; i++) {
            temp = i + 1;
            System.out.println("Enter player " + temp + "'s jersey number:");
            jersey[i] = scnr.nextInt();
            while (jersey[i] < 0 || jersey[i] > 99) {
                System.out.println("Enter player " + temp + "'s jersey number:");
                jersey[i] = scnr.nextInt();
            }
            System.out.println("Enter player " + temp + "'s rating:");
            rating[i] = scnr.nextInt();
            while (rating[i] < 1 || rating[i] > 9) {
                System.out.println("Enter player " + temp + "'s rating:");
                rating[i] = scnr.nextInt();
            }
            System.out.println("");
        }
        temp = 0;
        System.out.println("ROSTER");
        for (int j = 0; j < jersey.length; j++) {
            temp = j + 1;
            System.out.println("Player " + temp + " -- Jersey number: " + jersey[j] + ", Rating: " + rating[j]);
        }
        temp = 0;

        do {
            System.out.println("");
            System.out.println("MENU");
            System.out.println("u - Update player rating");
            System.out.println("a - Output players above a rating");
            System.out.println("r - Replace player");
            System.out.println("o - Output roster");
            System.out.println("q - Quit");
            System.out.println("");
            System.out.println("Choose an option:");
            menu = scnr.next().charAt(0);
            if (menu == 'o') {
                System.out.println("ROSTER");
                for (int j = 0; j < jersey.length; j++) {
                    temp = j + 1;
                    System.out.println("Player " + temp + " -- Jersey number: " + jersey[j] + ", Rating: " + rating[j]);
                }
            } else if (menu == 'u') {
                System.out.println("Enter a jersey number:");
                int update = scnr.nextInt();
                for (int i = 0; i < jersey.length; i++) {
                    if (update == jersey[i]) {
                        System.out.println("Enter a new rating for player:");
                        rating[i] = scnr.nextInt();
                        while (rating[i] < 1 || rating[i] > 9) {
                            System.out.println("Enter a new rating for player:");
                            rating[i] = scnr.nextInt();
                        }
                    }
                }
            }
            else if(menu == 'a'){
                temp = 0;
                System.out.println("Enter a rating:");
                int aboveRating = scnr.nextInt();
                System.out.println("");
                System.out.println("ABOVE " +aboveRating);
                for (int i = 0;i<rating.length;i++){
                    temp = i +1;
                    if(rating[i] > aboveRating){
                        System.out.println("Player " + temp + " -- Jersey number: " + jersey[i] + ", Rating: " + rating[i]);
                    }
                }
            }
            else if(menu == 'r'){
                System.out.println("Enter a jersey number:");
                int oldJer = scnr.nextInt();

                for (int i = 0;i < jersey.length;i++){
                    if(oldJer == jersey[i]){
                        System.out.println("Enter a new jersey number:");
                        int newJer = scnr.nextInt();
                        System.out.println("Enter a rating for the new player:");
                        int newRate = scnr.nextInt();
                        jersey[i] = newJer;
                        rating[i] = newRate;
                    }
                }

            }


        } while (menu != 'q');


    }
}
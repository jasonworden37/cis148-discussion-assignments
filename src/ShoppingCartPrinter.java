/*
   Name:
   Date:
*/

import java.util.Scanner;

public class ShoppingCartPrinter {
   public static void main(String[] args) {
      Scanner scnr = new Scanner(System.in);
      int i = 0;
      String productName;
      int productPrice = 0;
      int productQuantity = 0;
      int cartTotal = 0;
  

      // Get item 1 details from user
      System.out.println("Item 1");
      System.out.println("Enter the item name:");
      productName = scnr.nextLine();
      
      System.out.println("Enter the item price:");
      productPrice = scnr.nextInt();
      
      System.out.println("Enter the item quantity:");
      productQuantity = scnr.nextInt();
      System.out.println("");
      
      ItemToPurchase item1 = new ItemToPurchase(productName,productName,productPrice,productQuantity);
      
 
      // Get item 2 details from user
      System.out.println("Item 2");
      System.out.println("Enter the item name:");
      scnr.nextLine();
      productName = scnr.nextLine();
      
      System.out.println("Enter the item price:");
      productPrice = scnr.nextInt();
      
      System.out.println("Enter the item quantity:");
      productQuantity = scnr.nextInt();
      System.out.println("");

      ItemToPurchase item2 = new ItemToPurchase(productName,productPrice,productQuantity);
   

      // Add costs of two items and print total
      cartTotal = (item1.getPrice() * item1.getQuantity()) + 
                  (item2.getPrice() * item2.getQuantity());

      System.out.println("TOTAL COST");
      item1.printItemCost();
      item2.printItemCost();
      System.out.println("");
      System.out.println("Total: $" + cartTotal);
      
      return;
   }
}

import java.util.ArrayList;

public class ShoppingCart extends ItemToPurchase {
    private String customerName;
    private String currentDate;
    private ArrayList<ItemToPurchase> cartItems =new ArrayList<ItemToPurchase>();


    public ShoppingCart() {
        customerName = "none";
        currentDate = "January 1, 2016";
    }

    public ShoppingCart(String name, String date) {
        customerName = name;
        currentDate = date;
    }
    public String getDate(){
        return currentDate;
    }

    public String getCustomerName() {
        return customerName;
    }

    public String getCurrentDate() {
        return currentDate;
    }

    public void addItem(ItemToPurchase item1) {
        cartItems.add(item1);
    }

    public void removeItem(String name) {
        int count = 0;
        for (int i = 0; i < cartItems.size(); i++) {
           if (cartItems.get(i).getName().equals(name)){
               cartItems.remove(i);

           }
           else{
               count++;
           }
        }
            if(count == cartItems.size()){
                System.out.println("Item not found in cart. Nothing removed.");
            }
    }
    public void changeQuantity(String name, int quan){
        int count = 0;
        for (int i = 0; i < cartItems.size(); i++) {
            if (cartItems.get(i).getName().equals(name)){
                cartItems.get(i).setQuantity(quan);

            }
            else{
                count++;
            }
        }
        if(count == cartItems.size()){
            System.out.println("Item not found in cart. Nothing modified.");
        }
    }


    public int getNumItemsInCart() {
        int totalq = 0;
        for (int i = 0; i < cartItems.size(); i++) {
            totalq += cartItems.get(i).getQuantity();
        }
        return totalq;
    }



    public int getCostOfCart() {
        int totalp = 0;
        for (int i = 0; i < cartItems.size(); i++) {
            totalp += cartItems.get(i).getPrice() * cartItems.get(i).getQuantity();
        }
        return totalp;
    }
    public int printTotal() {
        if (cartItems.size() == 0) {
            System.out.println("SHOPPING CART IS EMPTY");
            return 0;
        } else {
            return cartItems.size();
        }
    }
    public void printDescription(){
        for (int i = 0; i < cartItems.size(); i++) {
            System.out.println(cartItems.get(i).getName() + ": " + cartItems.get(i).getDescription());
        }
    }
    public void printICost(){
        for (int i = 0; i < cartItems.size(); i++) {
            cartItems.get(i).printItemCost();
        }
    }
    public void modifyItem(ItemToPurchase item) {
        if (cartItems.contains(item.getName())) {
            if (!(item.getName().equals("none") && item.getPrice() == 0 && item.getQuantity() == 0 && item.getDescription().equals("none"))) {
                item.setName("none");
                item.setPrice(0);
                item.setQuantity(0);
                item.setDescription("none");

            }
        } else {
            System.out.println("Item not found in cart. Nothing modified.");
        }

    }
}
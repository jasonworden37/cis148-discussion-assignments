/*
   Name: Jason Worden
   Date: 09/03/2018
*/

import java.util.Scanner; 

public class OutputWithVars {
   public static void main(String[] args) {
      Scanner scnr = new Scanner(System.in);
      int userNum;
      int userNum2;
      
      System.out.println("Enter integer:");
      userNum = scnr.nextInt();  
      
      int squared = userNum * userNum;
      int cubed = squared * userNum;

      
      System.out.println("You entered: " +userNum);
      System.out.println( userNum +" squared is " +squared);
      System.out.println("And " +userNum +" cubed is " +cubed +"!!");
      System.out.println("Enter another integer:");
      userNum2 = scnr.nextInt();  
      
      int addNum = userNum + userNum2;
      int multNum = userNum * userNum2;

      System.out.println(userNum + " + " +userNum2 + " is " +addNum);
      System.out.println(userNum + " * " +userNum2 + " is " +multNum);
   }
}
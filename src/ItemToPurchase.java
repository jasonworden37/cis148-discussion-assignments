/*
   Name: 
   Date completed:
*/

public class ItemToPurchase {
   
   //***** Member variables *****//
   private String itemName;
   private int itemPrice;
   private int itemQuantity;
   private String itemDescription ;

   
   //***** Default Constructor *****//
   public ItemToPurchase() {
      itemName = "none";
      itemPrice = 0;
      itemQuantity = 0;
      itemDescription = "none";
   }
   public ItemToPurchase(String name,String description,int price, int quantity) {
      itemName = name;
      itemPrice = price;
      itemQuantity = quantity;
      itemDescription = description;
   }
   
   //***** Mutator methods *****//
   public void setName(String name) {
      itemName = name;
   }
   public void setPrice(int price) {
      itemPrice = price;
   }
   public void setQuantity(int quantity) {
      itemQuantity = quantity;
   }
   public void setDescription(String description) { itemDescription = description;
   }
   
   
   //***** Accessor methods *****//
   public String getName() {
      return itemName;
   }
   public int getPrice() {
      return itemPrice;
   }
   public int getQuantity() {
      return itemQuantity;
   }
   public String getDescription() {
      return itemDescription;
   }


   //***** Other methods *****//
   
   /*
      printItemCost will produce an output string that includes the item quantity, name, and price along with
      the total price for the item.
      
      Example:
      5 T-shirt $6 = $30
   */


   public void printItemCost() {
      System.out.println(getName() + " " + getQuantity() + " @ $" + getPrice() + " = $" +(getPrice()*getQuantity()));
   }
}

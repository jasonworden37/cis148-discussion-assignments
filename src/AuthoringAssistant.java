/*
   Name: Jason Worden
   Date: 10/16/2018
*/

import java.util.Scanner;

public class AuthoringAssistant {
   public static int getNumOfNonWSCharacters(String str){
        int numChar = 0;
        for(int i = 0; i<str.length();i++){
            if(str.charAt(i) != ' '){
                numChar++;
            }
        }
        return numChar;
    }
    public static int getNumOfWords(String str){
       int numWords = 0;
       str = shortenSpace(str);
       for(int i = 0; i<str.length();i++){
           if(str.charAt(i) == ' ') {
               numWords++;
           }
       }
        numWords++;
       return numWords;
    }
    public static int findText(String word, String str){

           int count = 0;
           count = str.split(word).length-1;

        return count;

    }
    public static String replaceExclamation(String str){

                str = str.replace('!','.');
        return str;
    }
    public static String shortenSpace(String str){
       while(str.contains("  ")) {
           str = str.replaceAll("  ", " ");
       }
        return str;
    }



    public static char printMenu(Scanner scnr) {
        char menuOp = '?';

        System.out.println("\nMENU");
        System.out.println( "c - Number of non-whitespace characters");
        System.out.println("w - Number of words");
        System.out.println("f - Find text");
        System.out.println("r - Replace all !\'s");
        System.out.println( "s - Shorten spaces");
        System.out.println("q - Quit\n");

        while (menuOp != 'c' && menuOp != 'w' && menuOp != 'f' &&
                menuOp != 'r' && menuOp != 's' && menuOp != 'o' &&
                menuOp != 'q') {
            System.out.println( "Choose an option:");
            menuOp = scnr.nextLine().charAt(0);
        }

        return menuOp;
    }
   
   


    public static void main(String[] args) {
        Scanner scnr = new Scanner(System.in);
        char menuChoice = '0';
        String f = " ";
        System.out.println("Enter a sample text:");
        String msg = scnr.nextLine();
        System.out.println();
        System.out.println("You entered: " +msg);



        while (menuChoice != 'q') {
            menuChoice = printMenu(scnr); //Make sure you understand what is happening here!

            if(menuChoice == 'c'){
                System.out.println("Number of non-whitespace characters: " +getNumOfNonWSCharacters(msg));
            }
            else if(menuChoice == 'w'){
                System.out.println("Number of words: " +getNumOfWords(msg));
            }
            else if(menuChoice == 'f'){
                System.out.println("Enter a word or phrase to be found:");
                f =scnr.nextLine();
                System.out.println("\"" +f +"\"" +   " instances: " +findText(f,msg));
            }
            else if(menuChoice == 'r'){
               System.out.println("Edited text: " +replaceExclamation(msg));
            }
            else if(menuChoice == 's'){
                System.out.println("Edited text: " + shortenSpace(msg));
            }

        }
    }
}
/*
   Name: 
   Date: 
*/

import java.util.Scanner;

public class TextMsgExpander {
    public static void main(String[] args) {
        Scanner scnr = new Scanner(System.in);
        String input = "Null";
        System.out.println("Enter text:");
        input = scnr.nextLine();
        System.out.println("You entered: " +input);
        System.out.println("");

        if (input.contains("BFF")){
            input = (input.replaceAll("BFF","best friend forever"));
            System.out.println("Replaced \"BFF\" with \"best friend forever\".");
        }
        if (input.contains("IDK")){
            input = (input.replaceAll("IDK","I don't know"));
            System.out.println("Replaced \"IDK\" with \"I don't know\".");
        }
        if (input.contains("JK")){
            input =(input.replaceAll("JK","just kidding"));
            System.out.println("Replaced \"JK\" with \"just kidding\".");
        }
        if (input.contains("TMI")){
            input =(input.replaceAll("TMI","too much information"));
            System.out.println("Replaced \"TMI\" with \"too much information\".");
        }
        if (input.contains("TTYL")){
            input =(input.replaceAll("TTYL","talk to you later"));
            System.out.println("Replaced \"TTYL\" with \"talk to you later\".");
        }
        System.out.println("");
        System.out.println("Expanded: " +input);




    }
}
/*
   Name: Jason Worden
   Date: 10/23/2018
*/

import java.util.Scanner;

public class TextAnalyzer {


    public static int getNumOfCharacters(final String target) {
        int numChar = 0;
        for(int i = 0; i<target.length();i++){

            numChar++;
        }
        return numChar;
    }


    public static String noWhiteSpace ( String target) {
        target = target.replaceAll(" ","");
        return target;
    }

    public static void main(String[] args) {
        Scanner scnr = new Scanner(System.in);
        String userString;

        System.out.println("Enter a sentence or phrase:");
        userString = scnr.nextLine();
        System.out.println("");
        System.out.println("You entered: " +userString);
        System.out.println("");
        System.out.println("Number of characters: " +getNumOfCharacters(userString));
        System.out.println("String with no whitespace: " +noWhiteSpace(userString));

    }
}
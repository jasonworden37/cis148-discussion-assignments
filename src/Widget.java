/*
Jason Worden
10/17/2018

This application will demonstrate the creation of a collection of objects. there will be
user input to create individual widget objects that contain
a name field and a price

Team Member with branch:
*/


public class Widget {
    // Object fields
    private String name;
    private double price;

    // Class fields
    private final static int MAX_WIDGETS = 5;
    private static int count = 0;

    // Argument-based Constructor
    public Widget(String name, double price) {
        this.name = name;
        this.price = price;
    }

    // No-argument Constructor
    public Widget() {
        this("Generic", 1.0);
    }

    // Object methods
    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice() {

    }

    // Class Methods
    public static int getMaxWidgets() {
        return MAX_WIDGETS;
    }

    public static int getCount() {
        return count;
    }

    public static void updateCount() {
        count += 1;
    }

    public String toString() {
        return "Name: " + name + " Price: $" + price;
    }

    protected void setName(String name) {
        this.name = name;
    }

}
import java.util.Scanner;

public class ShoppingCartManager extends ShoppingCart {
    public static void main(String[] args) {
        Scanner scnr = new Scanner(System.in);
        boolean q = false;

        String temp,rem;
        char ans = ' ';
        ShoppingCart cart1 = new ShoppingCart();
        System.out.println("Enter Customer's Name:");
        String cusName = scnr.nextLine();
        System.out.println("Enter Today's Date:");
        String date = scnr.nextLine();
        System.out.println("");
        System.out.println("Customer Name: " + cusName);
        System.out.println("Today's Date: " +date);
        System.out.println("");
        do {
            System.out.println("MENU");
            System.out.println("a - Add item to cart");
            System.out.println("d - Remove item from cart");
            System.out.println("c - Change item quantity");
            System.out.println("i - Output items' descriptions");
            System.out.println("o - Output shopping cart");
            System.out.println("q - Quit");
            System.out.println("");
        do {

            System.out.println("Choose an option:");
            temp = scnr.next();

            ans = temp.charAt(0);

            if (ans == 'a') {
                scnr.nextLine();
                System.out.println("");
                System.out.println("ADD ITEM TO CART");
                System.out.println("Enter the item name:");
                String name = scnr.nextLine();
                System.out.println("Enter the item description:");
                String des = scnr.nextLine();
                System.out.println("Enter the item price:");
                int price = scnr.nextInt();
                System.out.println("Enter the item quantity:");
                int quan = scnr.nextInt();
                System.out.println("");
                ItemToPurchase item = new ItemToPurchase(name, des, price, quan);
                cart1.addItem(item);
                item.setDescription(des);

            } else if (ans == 'd') {
                scnr.nextLine();
                System.out.println("REMOVE ITEM FROM CART");
                System.out.println("Enter name of item to remove:");
                 rem = scnr.nextLine();
                cart1.removeItem(rem);
                System.out.println("");
            } else if (ans == 'c') {
                scnr.nextLine();
                System.out.println("");
                System.out.println("CHANGE ITEM QUANTITY");
                System.out.println("Enter the item name:");
                String quanName = scnr.nextLine();
                System.out.println("Enter the new quantity:");
                int quanNew = scnr.nextInt();
                cart1.changeQuantity(quanName,quanNew);
                System.out.println("");


            } else if (ans == 'i') {
                System.out.println("");
                System.out.println("OUTPUT ITEMS' DESCRIPTIONS");
                System.out.println(cusName + "'s Shopping Cart - " + date);
                System.out.println("");
                System.out.println("Item Descriptions");
                cart1.printDescription();
                System.out.println("");
            } else if (ans == 'o') {

                System.out.println("OUTPUT SHOPPING CART");
                System.out.println(cusName + "'s Shopping Cart - " + date);
                System.out.println("Number of Items: " + cart1.getNumItemsInCart());
                System.out.println("");
                if(cart1.getNumItemsInCart() != 0){
                    cart1.printICost();
                }
                cart1.printTotal();
                System.out.println("");
                System.out.println("Total: $" +cart1.getCostOfCart());
                System.out.println("");

            } else if (ans == 'q') {

                q = true;
            }

        }while (ans != 'q' && ans != 'o' && ans != 'i' && ans != 'd' && ans != 'c' && ans != 'a' );
        }while (q != true);

    }
}

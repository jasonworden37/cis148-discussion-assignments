/*
Jason Worden
10/17/2018

This application will demonstrate the creation of a collection of objects. there will be
user input to create individual widget objects that contain
a name field and a price

Team Member with branch:
*/


import java.util.Scanner;

public class WidgetFactory {
    public static void main(String[] args) {
        Scanner scnr = new Scanner(System.in);
        String name;
        int price =0,count=0,average=0;
        Widget[] widgets = new Widget[Widget.getMaxWidgets()];
        /*
            - Setup Scanner object to read user input
            - use a loop to
                - prompt user to input a widget name and price
                - store values in a new widget
                - add the widget to a widget array
                - update widget count
            - You can only create up to the maximum number of widgets allowed
            - Display the collection of widgets and the average price
         */







        do{
            System.out.println("Input a name for this widget: ");
            name = scnr.next();
            System.out.println("Input a price for this widget: ");
            price = scnr.nextInt();
            widgets[count] = new Widget(name,price);Widget.updateCount();
            average += price;



            count++;
        }while(count < Widget.getMaxWidgets() );
        average /= Widget.getCount();
        for(int i = 0; i < Widget.getCount();i++){
            System.out.println(widgets[i]);
        }
        System.out.println("Average Price: " +average);

    }
}

